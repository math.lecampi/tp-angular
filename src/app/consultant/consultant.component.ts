import { Component, OnInit } from '@angular/core';
import { ConsultantService } from '../consultant.service';
import { Title } from '@angular/platform-browser';
import { Consultant } from '../model';

@Component({
  selector: 'app-consultant',
  templateUrl: './consultant.component.html',
  styleUrls: ['./consultant.component.css']
})
export class ConsultantComponent implements OnInit {

  tabConstultant:Consultant[]

  constructor(public consultantService: ConsultantService,private titleService: Title,) { }

  ngOnInit() {
    this.consultantService.list().subscribe(
      x => this.tabConstultant = x,
      err => console.error('Observer got an error: ' + err)
    );
  }

}
