import { Component, OnInit } from '@angular/core';
import { StartupService } from '../startup.service';
import { Startup } from '../model';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-startup',
  templateUrl: './startup.component.html',
  styleUrls: ['./startup.component.css']
})
export class StartupComponent implements OnInit {

  tabStartUp:Startup[];

  idCtrl: FormControl;
  nomCtrl: FormControl;
  secteurActiviteCtrl: FormControl;
  nomRepresentantLegalCtrl: FormControl;
  nbCoFondateursCtrl: FormControl;
  descriptionProjetCtrl: FormControl;
  adresseCtrl: FormControl;
  startupForm: FormGroup;

  isHiddenCreate = false;
  isHiddenUpdate = false;
  txtShowformulaire = "Ajouter une startup";

  currentStartUp:Startup;

  constructor(public startupService: StartupService,private titleService: Title, fb:FormBuilder) { 
    this.idCtrl = fb.control('');
    this.nomCtrl = fb.control('',[Validators.required,Validators.maxLength(20)]);
    this.secteurActiviteCtrl = fb.control('',[Validators.required,Validators.maxLength(10)]);
    this.nomRepresentantLegalCtrl = fb.control('',[Validators.required,Validators.maxLength(15)]);
    this.nbCoFondateursCtrl = fb.control('',[Validators.required,Validators.pattern("[0-9]+")]);
    this.descriptionProjetCtrl = fb.control('',[Validators.required,Validators.maxLength(250)]);
    this.adresseCtrl = fb.control('',[Validators.maxLength(25)]);

    this.startupForm = fb.group({
      id: this.idCtrl,
      nom: this.nomCtrl,
      secteurActivite: this.secteurActiviteCtrl,
      nomRepresentantLegal: this.nomRepresentantLegalCtrl,
      nbCoFondateurs: this.nbCoFondateursCtrl,
      descriptionProjet: this.descriptionProjetCtrl,
      adresse: this.adresseCtrl
    });

    this.titleService.setTitle("Liste des startup");
  }

  ngOnInit() {
    this.startupService.list().subscribe(
      x => this.tabStartUp = x,
      err => console.error('Observer got an error: ' + err)
    );
  }

  add(){
    let tmpAdresse = null;
    if(this.startupForm.value.adresse != ""){
      tmpAdresse = this.startupForm.value.adresse
    }
    let startUp:Startup = new Startup(
      this.startupForm.value.nom,
      this.startupForm.value.secteurActivite,
      this.startupForm.value.nomRepresentantLegal,
      this.startupForm.value.nbCoFondateurs,
      this.startupForm.value.descriptionProjet,
      null,
      tmpAdresse
    );
    this.startupService.add(startUp).subscribe(
      x => this.tabStartUp.push(x),
      error => console.log(error)
    );
    this.startupForm.reset();
    this.showFormulaireAddStartup();
  }

  reset(){
    this.startupForm.reset();
  }

  showFormulaireAddStartup(){
    if(this.isHiddenCreate){
      this.isHiddenCreate = false;
    }else{
      this.isHiddenCreate = true;
    }
  }

  showUpdateForm(id){
    this.startupService.getStartUp(id).subscribe(
      {
        next: startUp => {
          this.currentStartUp = startUp,
          this.idCtrl.setValue(this.currentStartUp.id);
          this.nomCtrl.setValue(this.currentStartUp.nom);
          this.secteurActiviteCtrl.setValue(this.currentStartUp.secteurActivite);
          this.nomRepresentantLegalCtrl.setValue(this.currentStartUp.nomRepresentantLegal);
          this.nbCoFondateursCtrl.setValue(this.currentStartUp.nbCoFondateurs);
          this.descriptionProjetCtrl.setValue(this.currentStartUp.descriptionProjet);
          this.adresseCtrl.setValue(this.currentStartUp.adresse);
          //this.startupForm.setValue(this.currentStartUp);
        },
      }
    );
    
    this.isHiddenUpdate = true;
  }

  updateCancel(){
    this.isHiddenUpdate = false;
  }

  update(){
    this.startupService.updateStartUp(this.startupForm.value).subscribe(
      {
        next: startUp => {
          this.startupService.list().subscribe({
            next: startUps => {
              startUps.filter(x => x.id == this.startupForm.value.id).map(eleve => eleve = this.startupForm.value)
            }
          }
          )
        },
      }
    );
    this.startupService.list().subscribe(
      x => this.tabStartUp = x,
      err => console.error('Observer got an error: ' + err)
    );
    this.currentStartUp = null;
    this.startupForm.reset();
    this.isHiddenUpdate = false;
  }

  delete(id){
    this.startupService.delete(id).subscribe(
      x => this.tabStartUp = this.tabStartUp.filter(y => y.id != id)
    )
  }
}
