import { Injectable } from '@angular/core';
import { Startup, Consultant } from './model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConsultantService {

  error:boolean;
  
  constructor(private http: HttpClient) { }

  list(): Observable<Consultant[]>{
    return this.http.get<Consultant[]>('api/consultants');
  }
}
