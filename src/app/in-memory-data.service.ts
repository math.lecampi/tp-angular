import { Injectable } from '@angular/core';
import { Startup,Consultant } from './model';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService {

  createDb() {
    const startups = [
      new Startup("Startup 1","Informatique","Lecampion",1,"Description de la startup 1",1),
      new Startup("Startup 2","Patiserie","Legal",2,"Description de la startup 1",2,"Chez elle"),
      new Startup("Startup 3","Animalerie","Lefeuvre",3,"Description de la startup 1",3),
      new Startup("Startup 4","Informatique","Boisson",1,"Description de la startup 1",4),
      new Startup("Startup 5","Bar","Pavée",5,"Description de la startup 1",5,"Chez elle"),
    ];

    const consultants = [
      new Consultant("Lemaitre","Thomas","Thomas le big boss",1),
      new Consultant("Lemaitre","Thomas","Thomas le big boss",2),
      new Consultant("Lemaitre","Thomas","Thomas le big boss",3),
      new Consultant("Lemaitre","Thomas","Thomas le big boss",4)
    ];
    
    return {startups,consultants};
   }

  genId<T extends Startup | Consultant>(table: T[]): number {
    return table.length > 0 ? Math.max(...table.map(t => t.id)) + 1 : 11;
  }

  constructor() { }
}
