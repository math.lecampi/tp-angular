import { Component, OnInit } from '@angular/core';
import { StartupService } from '../startup.service';
import { ConsultantService } from '../consultant.service';
import { Consultant, Startup } from '../model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  tabConsultant:Consultant[];
  tabStartUp:Startup[];

  constructor(public consultantService: ConsultantService,public startupService: StartupService) { }

  ngOnInit() {
    this.consultantService.list().subscribe(
      x => this.tabConsultant = x.slice(0,3),
      err => console.error('Observer got an error: ' + err)
    );

    this.startupService.list().subscribe(
      x => this.tabStartUp = x.slice(0,3),
      err => console.error('Observer got an error: ' + err)
    );

  }

}
