import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartupComponent } from './startup/startup.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ConsultantComponent } from './consultant/consultant.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'startup', component: StartupComponent},
  { path: 'consultant', component: ConsultantComponent},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
