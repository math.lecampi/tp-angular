import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nombrefondateur'
})
export class NombreFondateurPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    return (value > 1 ? (value > 2? 'Groupes' : 'Couple') : 'Unique');
  }

}