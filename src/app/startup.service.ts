import { Injectable } from '@angular/core';
import { Startup, Consultant } from './model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StartupService {

  error:boolean;
  
  constructor(private http: HttpClient) { }

  list(): Observable<Startup[]>{
    return this.http.get<Startup[]>('api/startups');
  }

  add(startup:Startup){
    return this.http.post<Startup>("api/startups",startup);
  }

  getStartUp(idStartUp): Observable<Startup>{
    return this.http.get<Startup>("api/startups/"+idStartUp);
  }

  delete(idStartUp){
    return this.http.delete<Startup>("api/startups/"+idStartUp);
  }

  updateStartUp(startUp:Startup){
    return this.http.put<Startup>("api/startups/"+startUp.id,startUp);
  }
}
