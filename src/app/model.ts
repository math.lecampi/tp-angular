export class Startup{

    id:number;
    nom:String;
    secteurActivite:String;
    nomRepresentantLegal:String;
    nbCoFondateurs:number;
    descriptionProjet:String;
    adresse:String;

    constructor(pNom:String,pSecteurActivité:String,pNomRepresentantLegal:String,pNbCoFondateurs:number,pDescriptionProjet:String,pId?:number,pAdresse?:String){
        this.id = pId;
        this.nom = pNom;
        this.secteurActivite = pSecteurActivité;
        this.nomRepresentantLegal = pNomRepresentantLegal;
        this.nbCoFondateurs = pNbCoFondateurs;
        this.descriptionProjet = pDescriptionProjet;
        this.adresse = pAdresse;
    }
}

export class Consultant{

    id:number
    nom:String;
    prenom:String;
    description:String;

    constructor(pNom:String,pPrenom:String,pDescription:String,pId:number){
        this.id = pId;
        this.nom = pNom;
        this.prenom = pPrenom;
        this.description = pDescription;
    }
}